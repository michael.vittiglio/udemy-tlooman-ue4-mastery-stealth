// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSBlackHoleActor.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AFPSBlackHoleActor::AFPSBlackHoleActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	EventHorizonSphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("EventHorizonSphereComponent"));
	GravityWellSphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("GravityWellSphereComponent"));
	RootComponent = MeshComponent;
	EventHorizonSphereComponent->SetupAttachment(MeshComponent);
	GravityWellSphereComponent->SetupAttachment(MeshComponent);

	MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	EventHorizonSphereComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	EventHorizonSphereComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	EventHorizonSphereComponent->SetCollisionResponseToChannel(ECC_PhysicsBody, ECR_Overlap);
	EventHorizonSphereComponent->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECR_Overlap);
	GravityWellSphereComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	GravityWellSphereComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	GravityWellSphereComponent->SetCollisionResponseToChannel(ECC_PhysicsBody, ECR_Overlap);
	GravityWellSphereComponent->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECR_Overlap);

	EventHorizonSphereComponent->OnComponentBeginOverlap.AddDynamic(this, &AFPSBlackHoleActor::OnEventHorizonEnter);

	GravityWellSphereComponent->InitSphereRadius(1000.0f);
	EventHorizonSphereComponent->InitSphereRadius(75.0f);
}

// Called when the game starts or when spawned
void AFPSBlackHoleActor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFPSBlackHoleActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	TArray<UPrimitiveComponent*> InGravityWell;
	GravityWellSphereComponent->GetOverlappingComponents(InGravityWell);

	FVector ActorLocation = GetActorLocation();
	float SphereRadius = GravityWellSphereComponent->GetScaledSphereRadius();
	ERadialImpulseFalloff Falloff = IsUsingLinearFalloff ? RIF_Linear : RIF_Constant;

	for(UPrimitiveComponent* ComponentInWell : InGravityWell)
	{
		ComponentInWell->SetSimulatePhysics(true);
		ComponentInWell->AddRadialForce(ActorLocation, SphereRadius, WellStrength, Falloff, true);
	}
}

void AFPSBlackHoleActor::OnEventHorizonEnter(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		if(EventHorizonHitParticleSystem)
		{
			UGameplayStatics::SpawnEmitterAtLocation(this, EventHorizonHitParticleSystem, OtherActor->GetActorLocation());
		}

		OtherActor->Destroy();
	}
}