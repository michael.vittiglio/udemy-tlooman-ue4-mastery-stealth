// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSAIGuardPatrolComponent.h"

#include "AI/Navigation/NavigationSystem.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/Controller.h"
#include "Perception/PawnSensingComponent.h"


// Sets default values for this component's properties
UFPSAIGuardPatrolComponent::UFPSAIGuardPatrolComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UFPSAIGuardPatrolComponent::BeginPlay()
{
	Super::BeginPlay();

	AActor* Owner = GetOwner();
	APawn* OwnerPawn = Cast<APawn>(Owner);
	if(OwnerPawn)
	{
		OwnerController = OwnerPawn->GetController();
	    MoveToNextPatrolPoint();
	}
}


// Called every frame
void UFPSAIGuardPatrolComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
	if(CurrentPatrolPoint)
	{
		FVector ToPatrolPoint = CurrentPatrolPoint->GetActorLocation() - GetOwner()->GetActorLocation();
		if(ToPatrolPoint.Size() < 50)
		{
			MoveToNextPatrolPoint();
		}
	}
}

void UFPSAIGuardPatrolComponent::MoveToNextPatrolPoint()
{
	if(CurrentPatrolPoint == nullptr || CurrentPatrolPoint == SecondPatrolPoint)
	{
		CurrentPatrolPoint = FirstPatrolPoint;
	}
	else if(CurrentPatrolPoint == FirstPatrolPoint)
	{
		CurrentPatrolPoint = SecondPatrolPoint;
	}

	ResumePatrol();
}

void UFPSAIGuardPatrolComponent::HaltPatrol()
{
	if(OwnerController)
	{
		OwnerController->StopMovement();
	}
}

void UFPSAIGuardPatrolComponent::ResumePatrol()
{
	if(CurrentPatrolPoint && OwnerController)
	{
		UNavigationSystem::SimpleMoveToActor(OwnerController, CurrentPatrolPoint);
	}
}