// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSLaunchPad.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"
#include "GameFramework/Actor.h"
#include "FPSCharacter.h"

// Sets default values
AFPSLaunchPad::AFPSLaunchPad()
{
	ColliderComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Collider"));
	DirectionalIndicatorComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Direction Indicator Mesh"));
	PlatformComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Platform Mesh"));

	RootComponent = PlatformComponent;
	ColliderComponent->SetupAttachment(PlatformComponent);
	DirectionalIndicatorComponent->SetupAttachment(PlatformComponent);

	DirectionalIndicatorComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	ColliderComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	ColliderComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	ColliderComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	ColliderComponent->SetCollisionResponseToChannel(ECC_PhysicsBody, ECR_Overlap);

	ColliderComponent->OnComponentBeginOverlap.AddDynamic(this, &AFPSLaunchPad::OnBeginOverlap);
}

void AFPSLaunchPad::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	FRotator ActorRotation = GetActorRotation();
	ActorRotation.Pitch += LaunchPitch;
	FVector LaunchVelocity = ActorRotation.Vector() * LaunchStrength;


	ACharacter* PlayerPawn = Cast<ACharacter>(OtherActor);
	if(PlayerPawn)
	{
		PlayerPawn->LaunchCharacter(LaunchVelocity, true, true);
		if(ParticleComponent)
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleComponent, GetTransform());
		}
	}
	else if(OtherComp && OtherComp->IsSimulatingPhysics())
	{
		OtherComp->AddImpulse(LaunchVelocity, NAME_None, true);
		if(ParticleComponent)
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleComponent, GetTransform());
		}
	}

}
