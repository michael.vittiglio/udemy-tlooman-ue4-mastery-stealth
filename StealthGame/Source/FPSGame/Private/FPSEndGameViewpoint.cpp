// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSEndGameViewpoint.h"
#include "Components/StaticMeshComponent.h"


// Sets default values
AFPSEndGameViewpoint::AFPSEndGameViewpoint()
{
	EditorRepresentation = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Editor Representation"));
	EditorRepresentation->SetHiddenInGame(true);
	RootComponent = EditorRepresentation;
}