// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSAIGuard.h"

#include "DrawDebugHelpers.h"
#include "TimerManager.h"
#include "Engine/World.h"
#include "GameFramework/Controller.h"
#include "AI/Navigation/NavigationSystem.h"
#include "Perception/PawnSensingComponent.h"

#include "FPSGameMode.h"
#include "FPSAIGuardPatrolComponent.h"


// Sets default values
AFPSAIGuard::AFPSAIGuard()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnSensingComponent = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComp"));
	PawnSensingComponent->OnHearNoise.AddDynamic(this, &AFPSAIGuard::OnNoiseHeard);
	PawnSensingComponent->OnSeePawn.AddDynamic(this, &AFPSAIGuard::OnPawnSeen);
	GuardState = EAIState::Idle;
	
	PatrolComponent = CreateDefaultSubobject<UFPSAIGuardPatrolComponent>(TEXT("Patrol Component"));
}

// Called when the game starts or when spawned
void AFPSAIGuard::BeginPlay()
{
	Super::BeginPlay();

	OriginalRotation = GetActorRotation();
}

void AFPSAIGuard::OnPawnSeen(APawn* SeenPawn)
{
	if(SeenPawn == nullptr)
	{
		return;
	}

	AFPSGameMode* GM = Cast<AFPSGameMode>(GetWorld()->GetAuthGameMode());
	if(GM)
	{
		GM->CompleteMission(SeenPawn, false);
	}

	if(bIsPatrolling)
	{
		PatrolComponent->HaltPatrol();
	}
	SetGuardState(EAIState::Alerted);
	DrawDebugSphere(GetWorld(), SeenPawn->GetActorLocation(), 32.0f, 12, FColor::Red, false, 10.0f);
}

void AFPSAIGuard::OnNoiseHeard(APawn* NoiseInstigator, const FVector& Location, float Loudness)
{
	if(NoiseInstigator == nullptr || GuardState == EAIState::Alerted)
	{
		return;
	}

	FVector ToNoise = Location - GetActorLocation();
	ToNoise.Normalize();
	FRotator RotationToNoise = FRotationMatrix::MakeFromX(ToNoise).Rotator();
	RotationToNoise.Pitch = 0;
	RotationToNoise.Roll = 0;

	SetActorRotation(RotationToNoise);
	SetGuardState(EAIState::Suspicious);
	if(bIsPatrolling)
	{
		PatrolComponent->HaltPatrol();
	}

	GetWorldTimerManager().ClearTimer(TimerHandle_ResetRotation);
	GetWorldTimerManager().SetTimer(TimerHandle_ResetRotation, this, &AFPSAIGuard::ReturnToIdleState, 3.0f, false);

	DrawDebugSphere(GetWorld(), Location, 32.0f, 12, FColor::Green, false, 10.0f);
}

void AFPSAIGuard::SetGuardState(EAIState NewState)
{
	if(NewState == GuardState)
	{
		return;
	}

	GuardState = NewState;

	OnGuardStateChange(GuardState);
}

void AFPSAIGuard::ReturnToIdleState()
{
	if(GuardState == EAIState::Alerted)
	{
		return;
	}

	SetGuardState(EAIState::Idle);
	SetActorRotation(OriginalRotation);
	if(bIsPatrolling)
	{
		PatrolComponent->ResumePatrol();
	}
}

// Called every frame
void AFPSAIGuard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
