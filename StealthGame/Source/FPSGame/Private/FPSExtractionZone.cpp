// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSExtractionZone.h"
#include "Components/BoxComponent.h"
#include "Components/DecalComponent.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"

#include "FPSCharacter.h"
#include "FPSGameMode.h"

// Sets default values
AFPSExtractionZone::AFPSExtractionZone()
{
	CollisionComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision Volume"));
	DecalComponent = CreateDefaultSubobject<UDecalComponent>(TEXT("Decal"));
	RootComponent = CollisionComponent;
	DecalComponent->SetupAttachment(RootComponent);

	CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	CollisionComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	CollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &AFPSExtractionZone::OnExtractionZoneEntered);

	FVector DefaultSize = FVector(200.0f);
	CollisionComponent->SetBoxExtent(DefaultSize);
	DecalComponent->DecalSize = DefaultSize;

	CollisionComponent->SetHiddenInGame(false);
}


void AFPSExtractionZone::OnExtractionZoneEntered(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AFPSCharacter* Player = Cast<AFPSCharacter>(OtherActor);
	if(!Player)
	{
		return;
	}

	if(Player->bIsCarryingObjective)
	{
		AFPSGameMode* GM = Cast<AFPSGameMode>(GetWorld()->GetAuthGameMode());
		if(GM)
		{
			GM->CompleteMission(Player, true);
		}
	}
	else
	{
		UGameplayStatics::PlaySound2D(this, ObjectiveUnobtainedErrorSound);
	}
}