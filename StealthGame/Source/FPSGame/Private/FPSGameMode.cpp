// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "FPSGameMode.h"

#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"

#include "FPSCharacter.h"
#include "FPSHUD.h"


AFPSGameMode::AFPSGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Blueprints/BP_Player"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AFPSHUD::StaticClass();
}


void AFPSGameMode::CompleteMission(APawn* PlayerPawn, bool bIsMissionComplete)
{
	if(PlayerPawn)
	{
		PlayerPawn->DisableInput(nullptr);

		TArray<AActor*> FoundViewpointActors;
		UGameplayStatics::GetAllActorsOfClass(this, ViewpointActorType, FoundViewpointActors);
		if(FoundViewpointActors.Num() >= 1)
		{
			if(FoundViewpointActors.Num() > 1)
			{
				UE_LOG(LogTemp, Warning, TEXT("Multiple viewpoints found to change camera angle."));
			}

			APlayerController* PlayerController = Cast<APlayerController>(PlayerPawn->GetController());
			if(PlayerController)
			{
				PlayerController->SetViewTargetWithBlend(FoundViewpointActors[0], 0.5f, EViewTargetBlendFunction::VTBlend_Cubic);
			}

		}
		else // FoundViewpointActors.Num() == 0
		{
			UE_LOG(LogTemp, Error, TEXT("No viewpoints found to change camera angle."));
		}

		OnMissionCompleted(PlayerPawn, bIsMissionComplete);
	}
}