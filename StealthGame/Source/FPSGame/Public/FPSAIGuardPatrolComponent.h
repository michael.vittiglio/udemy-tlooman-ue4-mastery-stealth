// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FPSAIGuardPatrolComponent.generated.h"

class AController;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FPSGAME_API UFPSAIGuardPatrolComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFPSAIGuardPatrolComponent();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	void MoveToNextPatrolPoint();

	UFUNCTION(BlueprintCallable)
	void HaltPatrol();

	UFUNCTION(BlueprintCallable)
	void ResumePatrol();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditInstanceOnly, Category = "AI")
	AActor* FirstPatrolPoint = nullptr;

	UPROPERTY(EditInstanceOnly, Category = "AI")
	AActor* SecondPatrolPoint = nullptr;

	AActor* CurrentPatrolPoint = nullptr;

private:
	AController * OwnerController = nullptr;
};
