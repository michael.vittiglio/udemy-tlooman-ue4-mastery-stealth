// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FPSEndGameViewpoint.generated.h"

class UMeshComponent;

UCLASS()
class FPSGAME_API AFPSEndGameViewpoint : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFPSEndGameViewpoint();

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Mesh Component")
	UStaticMeshComponent* EditorRepresentation = nullptr;
};