// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FPSBlackHoleActor.generated.h"

class UParticleSystem;
class UStaticMeshComponent;
class USphereComponent;

UCLASS()
class FPSGAME_API AFPSBlackHoleActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFPSBlackHoleActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void AddActorToGravityWell(AActor* OtherActor);

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UStaticMeshComponent* MeshComponent = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	USphereComponent* EventHorizonSphereComponent = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	USphereComponent* GravityWellSphereComponent = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UParticleSystem* EventHorizonHitParticleSystem = nullptr;

	UPROPERTY(EditAnywhere, Category = "GamePlay")
	bool IsUsingLinearFalloff = false;

	UPROPERTY(EditAnywhere, Category = "GamePlay")
	float WellStrength = -2000.0f;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnEventHorizonEnter(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
