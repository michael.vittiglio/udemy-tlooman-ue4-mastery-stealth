// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FPSGameMode.generated.h"

UCLASS()
class AFPSGameMode : public AGameModeBase
{
	GENERATED_BODY()
protected:
	UPROPERTY(EditDefaultsOnly, Category = "End game camera")
	TSubclassOf<AActor> ViewpointActorType;

public:

	AFPSGameMode();

	void CompleteMission(APawn* PlayerPawn, bool bIsMissionComplete);

	UFUNCTION(BlueprintImplementableEvent, Category = "Game Mode")
	void OnMissionCompleted(APawn* PlayerPawn, bool bIsMissionComplete);
};