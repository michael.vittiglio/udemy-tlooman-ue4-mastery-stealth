// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FPSLaunchPad.generated.h"

class UBoxComponent;
class UParticleSystem;
class UPrimitiveComponent;
class UStaticMeshComponent;

UCLASS()
class FPSGAME_API AFPSLaunchPad : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFPSLaunchPad();

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UParticleSystem* ParticleComponent = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UBoxComponent* ColliderComponent = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UStaticMeshComponent* DirectionalIndicatorComponent = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UStaticMeshComponent* PlatformComponent = nullptr;

	UPROPERTY(EditAnywhere, Category = "GamePlay")
	float LaunchPitch = 35.0f;

	UPROPERTY(EditAnywhere, Category = "GamePlay")
	float LaunchStrength = 1500.0f;

	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
